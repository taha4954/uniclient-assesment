
function reducer(state = {
    users: [
        {name: "user1", image: '/images/imgtest.jpg'},
        {name: "user2", image: '/images/imgtest.jpg'},
    

    ],


}, action: any) {

    switch (action.type) {
        case "handleUsersList":
            return {
                ...state,
                users: action.value
            };
        default:
            return state;
    }
}


export default reducer;
