import React, {Suspense} from 'react';
import ReactDOM from 'react-dom';
import './assets/styles/bootstrap.css';
import './assets/styles/global.scss';
import './assets/styles/public.scss';
import './assets/styles/dashboard.scss';
import './assets/styles/fontawesome.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import Loader from "./components/global/loaders/Loader";
import {Provider} from "react-redux";
import reducer from './redux/reducer'
import {createStore} from "redux";

const rdx = createStore(reducer);


ReactDOM.render(
    <React.StrictMode>
        <Provider store={rdx}>
            <Suspense fallback={<Loader/>}>
                <App/>
            </Suspense>
        </Provider>
    </React.StrictMode>,
    document.getElementById('root')
);

reportWebVitals();
