import React, {useEffect} from 'react';
import {Redirect, Route, Switch, HashRouter} from "react-router-dom";
import PublicLayout from "./components/public/main/PublicLayout";
import {useDispatch} from "react-redux";

const MainPage = React.lazy(() =>
    import( './pages/public/Main')
);

function App() {
    const dispatch = useDispatch();


    useEffect(() => {
        if (localStorage.getItem("user-list")) {


            let obj: any = localStorage.getItem("user-list");
            // let a = JSON.parse(localStorage.getItem("user-list"))
            dispatch({
                type: "handleUsersList",
                value: JSON.parse(obj)
            });
        }
    }, []);

    const AuthRoute = ({component: Component, ...rest}: any) => {

        return (
            <Route
                {...rest}
                render={(props) =>
                    localStorage.getItem("user-token") ?
                        <Component {...props} />
                        :
                        <Redirect
                            to={{
                                pathname: '/auth',
                                state: {from: props.location},
                            }}
                        />

                }
            />
        );
    };

    return (
        <div className="mainContainer">
            <HashRouter basename="/">

                <Switch>


                    <AuthRoute
                        // exact
                        path="/dashboard"
                        component={(props: any) =>
                            <div {...props}/>
                        }
                    />

                    <Route
                        path={"/auth"}
                        render={(props: any) => <div {...props}/>}
                    />

                    <Route
                        path={`/`}
                        render={(props: any) => <PublicLayout><MainPage {...props} /></PublicLayout>}
                    />

                </Switch>
            </HashRouter>
        </div>
    );
}

export default App;
