const BaseURL: string = "https://test.com";

interface GlobalAPIRoutesInter {
    Login: string;
    UserData: string;
    LeaderBoard: string;
    TimeLine: string;
}

export const GlobalAPIRoutes: GlobalAPIRoutesInter = {
    Login: `${BaseURL}/login`,
    UserData: `${BaseURL}/profile`,
    LeaderBoard: `${BaseURL}/leaderboard`,
    TimeLine: `${BaseURL}/timeline`,
};
