import React from 'react';
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";

const MainPage = React.lazy(() =>
    import( './Main')
);

const Main = () => {

    return (
        <Router>
            <Switch>
                <Route
                    path="/"
                    exact
                    component={(props: any) =>
                        <MainPage {...props}/>
                    }
                />

            </Switch>
        </Router>
    )
};

export default Main;
