import React from 'react';
import {useSelector} from "react-redux";
import UsersViewer from "../../components/public/main/UsersViewer";
import ChatBox from "../../components/public/main/ChatBox";

const Main = () => {

    const rstate: any = useSelector(state => state);

    return (
        <div>

            <div className=" justify-content-center col-12 hei-12">

               <div className="row  align-items-center">

                   <UsersViewer Users={rstate.users}/>

                   <ChatBox/>


               </div>

            </div>


        </div>
    )
};

export default Main;
