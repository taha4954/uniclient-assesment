import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import {SortableContainer, SortableElement, SortableHandle} from 'react-sortable-hoc';
import arrayMove from 'array-move';

const UsersViewer = ({Users}: any) => {

    const dispatch = useDispatch();

    const rstate: any = useSelector(state => state);

    const DragHandle = SortableHandle(() => <span className="position-absolute p-2 cursor-pointer" style={{top:3,left:3}}>:::</span>);


    const SortableList = SortableContainer(({items}: any) => {
        return (

            <div className="row col-xl-7 col-lg-7 col-md-12 col-12 colors" style={{paddingTop: '5vh'}}>
                {items.map((value: any, index: number) => (
                    <SortableItem key={`item-${value}`} index={index} name={value.name}
                                  value={process.env.PUBLIC_URL + value.image}/>
                ))}
            </div>

        );
    });

    const SortableItem = SortableElement(({value, name}: any) =>
        <div className="p-1 col-6  SortableItemCS columnCenter">
            <DragHandle/>
            <img style={{width: 'inherit'}} alt="" src={value}/>
            <span id="username">
                {name}
            </span>
        </div>
    );


    const onSortEnd = async ({oldIndex, newIndex}: any) => {
        let arrange: any = await arrayMove(Users, oldIndex, newIndex);

        dispatch({
            type: "handleUsersList",
            value: arrange
        });
        localStorage.setItem("user-list", JSON.stringify(arrange));

    };


    const addNewUser = async () => {

        if (rstate.users.length < 12) {
            let a = await rstate.users;
            a.push({name: `user${a.length+1}`, image: '/images/imgtest.jpg'});
            localStorage.setItem("user-list", JSON.stringify(a));
            dispatch({
                type: "handleUsersList",
                value: a
            });
        }


    };


    return (
        <div className="col-xl-9 col-lg-9 col-md-12 col-12  row justify-content-center align-items-center"
             style={{position: 'relative', overflowY: 'auto', height: "100vh"}}>


            <div className="col-12 row justify-content-center align-items-center  p-4 colors whiteColor"
                 style={{top: 0, position: 'sticky', zIndex: 2}}>

                UniClient

            </div>


            <SortableList
                useDragHandle
                axis="xy"
                items={Users}
                onSortEnd={onSortEnd}
            />


            <div className="col-12 row justify-content-center align-items-center  p-4"
                 style={{bottom: '5vh', position: 'sticky'}}>

                <i
                    className="fal fa-phone-alt colors redTwoBackColor whiteColor p-3 cursor-pointer"
                    style={{borderRadius: 50}}
                    onClick={addNewUser}
                />

            </div>

        </div>
    )
};

export default UsersViewer;
