import React from 'react';

const PublicLayout = ({children}: any) => {

    return (
        <>
            <main className="publicContainer">
                {children}
            </main>
        </>
    );

};

export default PublicLayout;