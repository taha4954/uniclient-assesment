import React from 'react';

const Loader = () => {

    return (
        <div className="loader">
            <div className="inner one"/>
            <div className="inner two"/>
            <div className="inner three"/>
        </div>
    );
};

export default Loader;
